<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "resep".
 *
 * @property int $resep_id
 * @property string $pasien_name
 * @property string $jenis
 * @property int $signa_id
 * @property int $obatalkes_id
 * @property string $created_date
 * @property int|null $created_by
 */
class Resep extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'resep';
    }

    public $qty_sisa;
    public $qty_max;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pasien_name', 'jenis', 'signa_id', 'obatalkes_id'], 'required'],
            [['signa_id', 'obatalkes_id', 'qty', 'created_by'], 'integer'],
            [['created_date'], 'safe'],
            [['pasien_name'], 'string', 'max' => 100],
            [['jenis'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'resep_id' => 'Resep ID',
            'pasien_name' => 'Pasien Name',
            'jenis' => 'Jenis',
            'signa_id' => 'Signa',
            'obatalkes_id' => 'Obat / Alkes',
            'qty' => 'Qty',
            'created_date' => 'Created Date',
            'created_by' => '',
        ];
    }

    public function getSigna()
    {
        return $this->hasOne(SignaM::class, ['signa_id' => 'signa_id']);
    }

    public function getObatalkes()
    {
        return $this->hasOne(ObatalkesM::class, ['obatalkes_id' => 'obatalkes_id']);
    }
}

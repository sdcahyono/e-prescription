<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Resep;

/**
 * ResepSearch represents the model behind the search form of `app\models\Resep`.
 */
class ResepSearch extends Resep
{
    public $nama_signa;
    public $nama_obatalkes;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['resep_id', 'signa_id', 'obatalkes_id', 'qty', 'created_by'], 'integer'],
            [['pasien_name', 'jenis', 'created_date', 'nama_signa', 'nama_obatalkes'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Resep::find();
        $query->leftJoin('signa_m', 'resep.signa_id = signa_m.signa_id');
        $query->leftJoin('obatalkes_m', 'resep.obatalkes_id = obatalkes_m.obatalkes_id');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'resep_id' => $this->resep_id,
            // 'signa_id' => $this->signa_id,
            // 'obatalkes_id' => $this->obatalkes_id,
            'qty' => $this->qty,
            'created_date' => $this->created_date,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'pasien_name', $this->pasien_name])
            ->andFilterWhere(['like', 'jenis', $this->jenis])
            ->andFilterWhere(['like', 'signa_m.signa_nama', $this->nama_signa])
            ->andFilterWhere(['like', 'obatalkes_m.obatalkes_nama', $this->nama_obatalkes]);

        return $dataProvider;
    }
}

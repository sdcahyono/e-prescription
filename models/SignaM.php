<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "signa_m".
 *
 * @property int $signa_id
 * @property string|null $signa_kode
 * @property string|null $signa_nama
 * @property string|null $additional_data
 * @property string $created_date
 * @property int|null $created_by
 * @property int|null $modified_count
 * @property string|null $last_modified_date
 * @property int|null $last_modified_by
 * @property int $is_deleted
 * @property int $is_active
 * @property string|null $deleted_date
 * @property int|null $deleted_by
 */
class SignaM extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'signa_m';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['additional_data'], 'string'],
            [['created_date', 'last_modified_date', 'deleted_date'], 'safe'],
            [['created_by', 'modified_count', 'last_modified_by', 'is_deleted', 'is_active', 'deleted_by'], 'integer'],
            [['signa_kode'], 'string', 'max' => 100],
            [['signa_nama'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'signa_id' => 'Signa ID',
            'signa_kode' => 'Signa Kode',
            'signa_nama' => 'Signa Nama',
            'additional_data' => 'Additional Data',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'modified_count' => 'Modified Count',
            'last_modified_date' => 'Last Modified Date',
            'last_modified_by' => 'Last Modified By',
            'is_deleted' => 'Is Deleted',
            'is_active' => 'Is Active',
            'deleted_date' => 'Deleted Date',
            'deleted_by' => 'Deleted By',
        ];
    }
}

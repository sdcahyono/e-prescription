<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "obatalkes_m".
 *
 * @property int $obatalkes_id
 * @property string|null $obatalkes_kode
 * @property string|null $obatalkes_nama
 * @property float|null $stok
 * @property string|null $additional_data
 * @property string $created_date
 * @property int|null $created_by
 * @property int|null $modified_count
 * @property string|null $last_modified_date
 * @property int|null $last_modified_by
 * @property int $is_deleted
 * @property int $is_active
 * @property string|null $deleted_date
 * @property int|null $deleted_by
 */
class ObatalkesM extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'obatalkes_m';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['stok'], 'number'],
            [['additional_data'], 'string'],
            [['created_date', 'last_modified_date', 'deleted_date'], 'safe'],
            [['created_by', 'modified_count', 'last_modified_by', 'is_deleted', 'is_active', 'deleted_by'], 'integer'],
            [['obatalkes_kode'], 'string', 'max' => 100],
            [['obatalkes_nama'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'obatalkes_id' => 'Obatalkes ID',
            'obatalkes_kode' => 'Obatalkes Kode',
            'obatalkes_nama' => 'Obatalkes Nama',
            'stok' => 'Stok',
            'additional_data' => 'Additional Data',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'modified_count' => 'Modified Count',
            'last_modified_date' => 'Last Modified Date',
            'last_modified_by' => 'Last Modified By',
            'is_deleted' => 'Is Deleted',
            'is_active' => 'Is Active',
            'deleted_date' => 'Deleted Date',
            'deleted_by' => 'Deleted By',
        ];
    }
}

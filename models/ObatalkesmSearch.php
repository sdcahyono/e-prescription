<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ObatalkesM;

/**
 * ObatalkesmSearch represents the model behind the search form of `app\models\ObatalkesM`.
 */
class ObatalkesmSearch extends ObatalkesM
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['obatalkes_id', 'created_by', 'modified_count', 'last_modified_by', 'is_deleted', 'is_active', 'deleted_by'], 'integer'],
            [['obatalkes_kode', 'obatalkes_nama', 'additional_data', 'created_date', 'last_modified_date', 'deleted_date'], 'safe'],
            [['stok'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ObatalkesM::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'obatalkes_id' => $this->obatalkes_id,
            'stok' => $this->stok,
            'created_date' => $this->created_date,
            'created_by' => $this->created_by,
            'modified_count' => $this->modified_count,
            'last_modified_date' => $this->last_modified_date,
            'last_modified_by' => $this->last_modified_by,
            'is_deleted' => $this->is_deleted,
            'is_active' => $this->is_active,
            'deleted_date' => $this->deleted_date,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'obatalkes_kode', $this->obatalkes_kode])
            ->andFilterWhere(['like', 'obatalkes_nama', $this->obatalkes_nama])
            ->andFilterWhere(['like', 'additional_data', $this->additional_data]);

        return $dataProvider;
    }
}

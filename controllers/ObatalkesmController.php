<?php

namespace app\controllers;

use app\models\ObatalkesM;
use app\models\ObatalkesmSearch;
use app\models\SignaM;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * ObatalkesmController implements the CRUD actions for ObatalkesM model.
 */
class ObatalkesmController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all ObatalkesM models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ObatalkesmSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ObatalkesM model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ObatalkesM model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ObatalkesM();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->obatalkes_id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ObatalkesM model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->obatalkes_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ObatalkesM model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionGetQty($obatalkesId)
    {
        $qty = ObatalkesM::findOne($obatalkesId);
        echo Json::encode($qty);
    }

    public function actionGetLihat($nama_pasien, $signa, $obat, $qty)
    {
        $nama_signa = SignaM::find()->select('signa_nama')->where(['signa_id' => $signa])->one();
        $nama_obat = ObatalkesM::find()->select('obatalkes_nama')->where(['obatalkes_id' => $obat])->one();
?>
        <!-- <div class="row"> -->
        <div class="col-xs-5">
            <h3 align="center">Resep</h3>
            <table class="table">
                <!-- <thead>
                <tr>
                    <th scope="col">Nama Pasien</th>
                    <th scope="col">Signa</th>
                    <th scope="col">Obat</th>
                    <th scope="col">Jml</th>
                </tr>
            </thead> -->
                <tr>
                    <th scope="row" width="30%">Pasien</th>
                    <td><?= $nama_pasien; ?></td>
                </tr>
                <tr>
                    <th scope="row">Signa</th>
                    <td><?= $nama_signa->signa_nama; ?></td>
                </tr>
                <tr>
                    <th scope="row">Obat</th>
                    <td><?= $nama_obat->obatalkes_nama; ?></td>
                </tr>
                <tr>
                    <th scope="row">Jumlah</th>
                    <td><?= $qty; ?></td>
                </tr>
            </table>
        </div>
        <!-- </div> -->
<?php
    }

    /**
     * Finds the ObatalkesM model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ObatalkesM the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ObatalkesM::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

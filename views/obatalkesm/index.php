<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ObatalkesmSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Obatalkes Ms';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="obatalkes-m-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Obatalkes M', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'obatalkes_id',
            'obatalkes_kode',
            'obatalkes_nama',
            'stok',
            'additional_data:ntext',
            //'created_date',
            //'created_by',
            //'modified_count',
            //'last_modified_date',
            //'last_modified_by',
            //'is_deleted',
            //'is_active',
            //'deleted_date',
            //'deleted_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
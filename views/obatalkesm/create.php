<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ObatalkesM */

$this->title = 'Create Obatalkes M';
$this->params['breadcrumbs'][] = ['label' => 'Obatalkes Ms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="obatalkes-m-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
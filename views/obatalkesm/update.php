<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ObatalkesM */

$this->title = 'Update Obatalkes M: ' . $model->obatalkes_id;
$this->params['breadcrumbs'][] = ['label' => 'Obatalkes Ms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->obatalkes_id, 'url' => ['view', 'id' => $model->obatalkes_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="obatalkes-m-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SignamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Signa Ms';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="signa-m-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Signa M', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'signa_id',
            'signa_kode',
            'signa_nama',
            'additional_data:ntext',
            'created_date',
            //'created_by',
            //'modified_count',
            //'last_modified_date',
            //'last_modified_by',
            //'is_deleted',
            //'is_active',
            //'deleted_date',
            //'deleted_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
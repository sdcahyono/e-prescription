<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SignaM */

$this->title = 'Update Signa M: ' . $model->signa_id;
$this->params['breadcrumbs'][] = ['label' => 'Signa Ms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->signa_id, 'url' => ['view', 'id' => $model->signa_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="signa-m-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
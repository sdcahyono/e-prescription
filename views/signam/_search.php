<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SignamSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="signa-m-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'signa_id') ?>

    <?= $form->field($model, 'signa_kode') ?>

    <?= $form->field($model, 'signa_nama') ?>

    <?= $form->field($model, 'additional_data') ?>

    <?= $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'created_by') 
    ?>

    <?php // echo $form->field($model, 'modified_count') 
    ?>

    <?php // echo $form->field($model, 'last_modified_date') 
    ?>

    <?php // echo $form->field($model, 'last_modified_by') 
    ?>

    <?php // echo $form->field($model, 'is_deleted') 
    ?>

    <?php // echo $form->field($model, 'is_active') 
    ?>

    <?php // echo $form->field($model, 'deleted_date') 
    ?>

    <?php // echo $form->field($model, 'deleted_by') 
    ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
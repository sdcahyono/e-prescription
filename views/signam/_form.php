<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SignaM */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="signa-m-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'signa_kode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'signa_nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'additional_data')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'created_date')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'modified_count')->textInput() ?>

    <?= $form->field($model, 'last_modified_date')->textInput() ?>

    <?= $form->field($model, 'last_modified_by')->textInput() ?>

    <?= $form->field($model, 'is_deleted')->textInput() ?>

    <?= $form->field($model, 'is_active')->textInput() ?>

    <?= $form->field($model, 'deleted_date')->textInput() ?>

    <?= $form->field($model, 'deleted_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SignaM */

$this->title = $model->signa_id;
$this->params['breadcrumbs'][] = ['label' => 'Signa Ms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="signa-m-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->signa_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->signa_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'signa_id',
            'signa_kode',
            'signa_nama',
            'additional_data:ntext',
            'created_date',
            'created_by',
            'modified_count',
            'last_modified_date',
            'last_modified_by',
            'is_deleted',
            'is_active',
            'deleted_date',
            'deleted_by',
        ],
    ]) ?>

</div>
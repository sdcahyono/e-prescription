<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SignaM */

$this->title = 'Create Signa M';
$this->params['breadcrumbs'][] = ['label' => 'Signa Ms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="signa-m-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Resep */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="resep-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'pasien_name')->textInput(['maxlength' => true]) ?>

    <!-- <?= $form->field($model, 'jenis')->textInput(['maxlength' => true]) ?> -->

    <?= $form->field($model, 'jenis')->dropDownList(['Non racikan' => 'Non racikan', 'Racikan' => 'Racikan'], ['prompt' => '- pilih jenis -']);
    ?>

    <!-- <?= $form->field($model, 'signa_id')->textInput() ?> -->

    <?php
    $dataPost = ArrayHelper::map(\app\models\SignaM::find()->asArray()->all(), 'signa_id', 'signa_nama');
    echo $form->field($model, 'signa_id')->dropDownList($dataPost, ['prompt' => '- pilih signa -']);
    ?>

    <!-- <?= $form->field($model, 'obatalkes_id')->textInput() ?> -->

    <!-- <?php $dataPost = ArrayHelper::map(\app\models\ObatalkesM::find()->asArray()->where(['not in', 'stok', [0]])->all(), 'obatalkes_id', 'obatalkes_nama');
            echo $form->field($model, 'obatalkes_id')->dropDownList($dataPost, ['obatalkes_id' => 'obatalkes_nama']);
            ?> -->
    <?php $dataPost = ArrayHelper::map(\app\models\ObatalkesM::find()->asArray()->where(['not in', 'stok', [0]])->all(), 'obatalkes_id', 'obatalkes_nama');
    echo $form->field($model, 'obatalkes_id')->dropDownList($dataPost, ['prompt' => '- pilih obat / alkes -']);
    ?>

    <?= $form->field($model, 'qty')->textInput() ?>

    <?= $form->field($model, 'qty_max')->textInput(['id' => 'qtymax', 'readonly' => 'readonly']) ?>

    <?= $form->field($model, 'qty_sisa')->textInput(['id' => 'qtysisa', 'readonly' => 'readonly']) ?>

    <!-- <?= $form->field($model, 'created_date')->textInput() ?> -->

    <?= $form->field($model, 'created_by')->textInput(['type' => 'hidden']) ?>

    <div class="form-group">
        <?= Html::button('Lihat Resep', ['class' => 'btn btn-success', 'id' => 'lihat']) ?>
    </div>
    <div id="hasil"></div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
$('#resep-obatalkes_id').change(function(){
    var obatalkesId = $(this).val();

    $.get('index.php?r=obatalkesm/get-qty',{obatalkesId : obatalkesId}, function(datas){
        var data = $.parseJSON(datas);
        // alert(data.stok);
        var stok = data.stok;
        // if (stok==0) {
        //     $('#resep-obatalkes_id').attr('value','');
        // }
        $('#qtymax').attr('value',data.stok);
    })
});

$('#resep-qty').keyup(function(){
    var qty = parseInt($('#resep-qty').val());
    var max = parseInt($('#qtymax').val());
    var sisa = max-qty;
    // alert(sisa);
    if (qty>max) {
        alert("Tidak boleh lebih dari stok");
        $('#resep-qty').focus();

    }
    // $('#qtysisa').attr('value',sisa);
})

$('#resep-qty').blur(function(){
    var qty = parseInt($('#resep-qty').val());
    var max = parseInt($('#qtymax').val());
    var sisa = max-qty;
    // alert(sisa);
    $('#qtysisa').attr('value',sisa);
})

$('#lihat').click(function(){
    var nama_pasien = $('#resep-pasien_name').val();
    var jenis_resep = $('#resep-jenis').val();
    var signa = $('#resep-signa_id').val();
    var obat = $('#resep-obatalkes_id').val();
    var qty = parseInt($('#resep-qty').val());
    $.get('index.php?r=obatalkesm/get-lihat',{nama_pasien : nama_pasien, signa : signa, obat : obat, qty : qty}, function(datas){
        // var data = $.parseJSON(datas);
        // alert(data.stok);
        // var stok = data.stok;
        // if (stok==0) {
        //     $('#resep-obatalkes_id').attr('value','');
        // }
        $('#hasil').html(datas);
    })
})
JS;
$this->registerJs($script);
?>